@extends('layouts.app')

@section('header')
	<section class="content-header">
		<h1>
		Dashboard
		<small>Merchant</small>
		</h1>
		<ol class="breadcrumb">
			<li class="active"><a href="#"><i class="fa fa-dashboard"></i> Dashboard</a></li>
		</ol>
	</section>
@endsection
